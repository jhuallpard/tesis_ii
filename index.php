<?php

// Implementacion del login del sistema

  $alert= '';
  session_start();

  if (!empty($_SESSION['active'])) {
      header('location: sistema/');
  }else{
    if(!empty($_POST)){

      if(empty($_POST['usuario']) || empty($_POST['clave'])){
        $alert= 'Ingrese su usuario y contraseña';
      }else{

        require_once "conexion.php";

        $user= mysqli_real_escape_string($conection, $_POST['usuario']);
        $pass= md5(mysqli_real_escape_string($conection,$_POST['clave']));

        $query= mysqli_query($conection, "SELECT * FROM usuarios WHERE usuario= '$user' AND clave= '$pass'");
        $result= mysqli_num_rows($query);

        if($result > 0){
          $data= mysqli_fetch_array($query);
          $_SESSION['active']= true;
          $_SESSION['idUser']= $data['id_usuario'];
          $_SESSION['nombre']= $data['nombre'];
          $_SESSION['email'] = $data['correo'];
          $_SESSION['user']  = $data['usuario'];

          header('location: sistema/');

        }else{
          $alert= 'El usuario o la clave son incorrecta';
          session_destroy();
        }
      }
    }
  }
?>

<!DOCTYPE html>
<html>
    
<head>
  <meta charset="utf-8">
  <title>My Sistem</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<!--Coded with love by Mutiullah Samim-->
<body>
  <section id="container">
    <form action="" method="post">
      <h3>Iniciar Sesión</h3>
      <img src="sistema/img/login.png" class="brand_logo" alt="Logo">

     <input type="text" name="usuario" placeholder="Usuario">
     <input type="password" name="clave" placeholder="Contraseña">
     <div class="alert"><?php echo isset($alert) ? $alert: ''; ?></div>
     <input type="submit" value="INGRESAR">
    </form>
  </section>

</body>
</html>