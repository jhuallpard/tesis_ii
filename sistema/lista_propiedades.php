<?php
	include "../conexion.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Lista de Propiedades</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<h1>Lista de Propiedades</h1>
		<a href="registro_propiedad.php" class="btn_new"> Crear Propiedad</a>
		<table>
			<tr>
				<th>ID</th>
				<th>Dirección</th>
				<th>Descripción</th>
				<th>Categoría</th>
				<th>Sector</th>
				<th>Propietario</th>
				<th>Estado</th>
				<th>Acciones</th>
			</tr>
			<?php

				$query= mysqli_query($conection, "SELECT p.id_propiedad, p.direccion, p.descripcion, (cat.nombre) as categoria, (sec.nombre) as sector, (cli.nombre) as cliente, p.estado FROM propiedades p 
					INNER JOIN categorias cat ON p.id_categoria = cat.id_categoria
					INNER JOIN sectores sec ON p.id_sector = sec.id_sector
					INNER JOIN clientes cli ON p.id_cliente = cli.id_cliente");
				$result= mysqli_num_rows($query);

				if ($result > 0) {
					// code...
					while ($data= mysqli_fetch_array($query)) {
						// code...
			?>
			<tr>
				<td><?php echo $data["id_propiedad"]; ?></td>
				<td><?php echo $data["direccion"]; ?></td>
				<td><?php echo $data["descripcion"]; ?></td>
				<td><?php echo $data["categoria"]; ?></td>
				<td><?php echo $data["sector"]; ?></td>
				<td><?php echo $data["cliente"]; ?></td>
				<td><?php echo $data["estado"]; ?></td>
				<td>
					<a class="link_edit" href="editar_propiedad.php?id=<?php echo $data["id_propiedad"]; ?>">Editar</a>
					|
					<a class="link_delete" href="eliminar_confirmar_propiedad.php?id=<?php echo $data["id_propiedad"]; ?>">Eliminar</a>
				</td>
			</tr>

			<?php
					}
				}
			?>
		</table>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>