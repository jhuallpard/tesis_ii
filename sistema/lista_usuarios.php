<?php
	include "../conexion.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Lista de Usuarios</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<h1>Lista de Usuarios</h1>
		<a href="registro_usuario.php" class="btn_new"> Crear Usuario</a>
		<table>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Usuario</th>
				<th>Rol</th>
				<th>Acciones</th>
			</tr>
			<?php

				$query= mysqli_query($conection, "SELECT u.id_usuario, u.nombre, u.correo, u.usuario, r.rol FROM usuarios u INNER JOIN rol r ON u.rol = r.id_rol WHERE status= 1");
				$result= mysqli_num_rows($query);

				if ($result > 0) {
					// code...
					while ($data= mysqli_fetch_array($query)) {
						// code...


			?>
			<tr>
				<td><?php echo $data["id_usuario"]; ?></td>
				<td><?php echo $data["nombre"]; ?></td>
				<td><?php echo $data["correo"]; ?></td>
				<td><?php echo $data["usuario"]; ?></td>
				<td><?php echo $data["rol"]; ?></td>
				<td>
					<a class="link_edit" href="editar_usuario.php?id=<?php echo $data["id_usuario"]; ?>">Editar</a>
					|
					<?php
						if ($data["id_usuario"]!=1) { ?>
					<a class="link_delete" href="eliminar_confirmar_usuario.php?id=<?php echo $data["id_usuario"]; ?>">Eliminar</a>
					<?php } ?>
				</td>
			</tr>

			<?php
					}
				}
			?>
		</table>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>