<?php
	include "../conexion.php";
	if (!empty($_POST)) {
	 	// code...
	 	$alert='';
	 	if (empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['usuario']) || empty($_POST['rol'])) {
	 		// code...
	 		$alert= '<p class="msg_error">Todos los campos son obligatorios</p>';
	 	}else{
	 		$idusuario = $_POST['id_usuario'];
	 		$nombre = $_POST['nombre'];
	 		$email = $_POST['correo'];
	 		$user = $_POST['usuario'];
	 		$clave = md5($_POST['clave']);
	 		$rol = $_POST['rol'];

	 		$query= mysqli_query($conection, "SELECT * FROM usuarios where (usuario= '$user' AND id_usuario!= $idusuario) OR (correo= '$email' AND id_usuario!= $idusuario)");

	 		$result= mysqli_fetch_array($query);

	 		if ($result > 0) {
	 			// code...
	 			$alert= '<p class="smg_error"> El correo o usuario ya existe.</p>';
	 		}else{
	 			if (empty($_POST['clave'])) {
	 				// code...
	 				$sql_update= mysqli_query($conection, "UPDATE usuarios SET nombre= '$nombre', correo= '$email', usuario= '$user', rol= '$rol' WHERE id_usuario= '$idusuario'");

	 			}else{ $sql_update= mysqli_query($conection, "UPDATE usuarios SET nombre= '$nombre', correo= '$email', usuario= '$user', clave='$clave', rol= '$rol' WHERE id_usuario= '$idusuario'");

	 			}

	 			if ($sql_update) {
	 				// code...
	 				$alert= '<p class="smg_save"> Usuario actualizado correctamente</p>';
	 				header('location: lista_usuarios.php');
	 			}else{
	 				$alert= '<p class="smg_save"> Error al actualizar el Usuario</p>';
	 			}
	 		}
	 	}
	 } 

	 //Mostrar Usuarios
	 if (empty($_GET['id'])) {
	 	// code...
	 	header('location: lista_usuarios.php');
	 }
	 $iduser=$_GET['id'];

	 $sql= mysqli_query($conection, "SELECT u.id_usuario, u.nombre, u.correo, u.usuario, (u.rol) as idrol, (r.rol) as rol FROM usuarios u INNER JOIN rol r on u.rol=r.id_rol WHERE id_usuario= $iduser");

	 $result_sql= mysqli_num_rows($sql);

	 if ($result_sql == 0) {
	 	// code...
	 	header('location: lista_usuarios.php');
	 }else{
	 	$option= ' ';
	 	while ($data= mysqli_fetch_array($sql)) {
	 		// code...
	 		$iduser = $data['id_usuario'];
	 		$nombre = $data['nombre'];
	 		$email = $data['correo'];
	 		$user = $data['usuario'];
	 		$idrol= $data['idrol'];
	 		$rol = $data['rol'];

	 		if ($idrol == 1) {
	 			$option= '<option value="'.$idrol.'" select>'.$rol.'</option>';
	 		}
	 		else if($idrol == 2){
	 			$option= '<option value="'.$idrol.'" select>'.$rol.'</option>';
	 		}
	 	}
	 }
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Actualizar Usuario</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<div class="form_register">
			<h1>Actualizar Usuario</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert:''; ?></div>

			<form action="" method="post">
				<input type="hidden" name="id_usuario" value="<?php echo $iduser; ?>">
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" placeholder="Nombre Completo" value="<?php echo $nombre; ?>">

				<label for="correo">Correo electrónico</label>
				<input type="email" name="correo" id="correo" placeholder="Correo Electrónico" value="<?php echo $email; ?>">

				<label for="usuario">Usuario</label>
				<input type="text" name="usuario" id="usuario" placeholder="Usuario" value="<?php echo $user; ?>">

				<label for="clave">Contraseña</label>
				<input type="password" name="clave" id="clave" placeholder="Contraseña de acceso">

				<label for="rol">Tipo de usuario</label>
				<?php
					$query_rol= mysqli_query($conection, "SELECT * FROM rol");
					$result_rol= mysqli_num_rows($query_rol);
				?>

				<select name="rol" id="rol" class ="notItemOne">
					<?php
						echo $option;
						if ($result_rol > 0) {
							while ($rol = mysqli_fetch_array($query_rol)) {
					?>
								<option value="<?php echo $rol["id_rol"]; ?>"><?php echo $rol["rol"]; ?></option>
					<?php
							}
						}
					?>
				</select>
				<input type="submit" value="Actualizar usuario" class="btn_save">
			</form>
		</div>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>