<?php
	include "../conexion.php";
	if (!empty($_POST)) {
	 	// code...
	 	$alert='';
	 	if (empty($_POST['fecha_emision']) || empty($_POST['fecha_caducidad']) || empty($_POST['mes']) || empty($_POST['descripcion']) || empty($_POST['id_propiedad']) || empty($_POST['id_usuario'])) {
	 		// code...
	 		$alert= '<p class="msg_error">Todos los campos son obligatorios</p>';
	 	}else{

	 		$fecha_emision = $_POST['fecha_emision'];
	 		$fecha_caducidad = $_POST['fecha_caducidad'];
	 		$mes = $_POST['mes'];
	 		$descripcion = $_POST['descripcion'];
	 		$propiedad = $_POST['id_propiedad'];
	 		$usuario = $_POST['id_usuario'];

	 		$query= mysqli_query($conection, "SELECT * FROM recibos where id_propiedad='$propiedad'");
	 		$result= mysqli_fetch_array($query);

	 		if ($result > 0) {
	 			// code...
	 			$alert= '<p class="smg_error">El recibo ya existe</p>';
	 		}else{
	 			$query_insert= mysqli_query($conection, "INSERT INTO recibos(fecha_emision, fecha_caducidad, mes, descripcion, id_propiedad, id_usuario) VALUES ('$fecha_emision','$fecha_caducidad','$mes','$descripcion','$propiedad','$usuario')");
	 			if ($query_insert) {
	 				// code...
	 				$alert= '<p class="smg_save"> Recibo creado correctamente</p>';
	 				header('location: lista_recibos.php');
	 			}else{
	 				$alert= '<p class="smg_save"> Error al crear el Recibo</p>';
	 			}
	 		}
	 	}
	 	//mysql_close($conection);
	 }
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Registro Recibo</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<div class="form_register">
			<h1>Registro Recibo</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert:''; ?></div>

			<form action="" method="post">
				<label for="id_propiedad">Nombre del usuario:</label>
				<?php
					$query_cliente= mysqli_query($conection, "SELECT (p.id_propiedad) as id_propiedad, (p.direccion) as direccion, (c.nombre) as nombre_cliente FROM propiedades p INNER JOIN clientes c ON p.id_cliente=c.id_cliente");
					$result_cliente= mysqli_num_rows($query_cliente);
				?>

				<select name="id_propiedad" id="id_propiedad">
					<?php 
						if ($result_cliente > 0) {
							while ($propiedad = mysqli_fetch_array($query_cliente)) {
					?>
								<option value="<?php echo $propiedad["id_propiedad"]; ?>"><?php echo $propiedad["nombre_cliente"]; ?></option>
					<?php
							}
						}
					?>
				</select>

				<!--<label for="id_propiedad">Dirección:</label>
				<input type="text" name="id_propiedad" id="id_propiedad" placeholder="Ingrese la Dirección" value="">

				<label for="id_propiedad">Categoria - Precio:</label>
				<input type="text" name="id_propiedad" id="id_propiedad" placeholder="Ingrese la categoria - precio">

				<label for="id_propiedad">Sector:</label>
				<input type="text" name="id_propiedad" id="id_propiedad" placeholder="Ingrese el sector">-->

				<label for="mes">Mes de consumo:</label>

				<select name="mes" id="mes">
					<option value="Enero">Enero</option>
					<option value="Febrero">Febrero</option>
					<option value="Marzo">Marzo</option>
					<option value="Abril">Abril</option>
					<option value="Mayo">Mayo</option>
					<option value="Junio">Junio</option>
					<option value="Julio">Julio</option>
					<option value="Agosto">Agosto</option>
					<option value="Septiembre">Septiembre</option>
					<option value="Octubre">Octubre</option>
					<option value="Noviembre">Noviembre</option>
					<option value="Diciembre">Diciembre</option>
				</select>

				<label for="fecha_emision">Fecha de emisión:</label>
				<input type="date" name="fecha_emision" id="fecha_emision" placeholder="Ingrese la fecha de emisióm">

				<label for="fecha_caducidad">Fecha de caducidad:</label>
				<input type="date" name="fecha_caducidad" id="fecha_caducidad" placeholder="Ingrese la fecha de caducidad">


				<label for="descripcion">Descripcion:</label>
				<textarea name="descripcion" id="descripcion" placeholder="Ingrese la Descripción" rows="10" cols="40"></textarea>

				<label for="id_usuario">Administrador:</label>
				<?php
					$query_admin= mysqli_query($conection, "SELECT * FROM usuarios");
					$result_admin= mysqli_num_rows($query_admin);
				?>

				<select name="id_usuario" id="id_usuario">
					<?php 
						if ($result_admin > 0) {
							while ($admin = mysqli_fetch_array($query_admin)) {
					?>
								<option value="<?php echo $admin["id_usuario"]; ?>"><?php echo $admin["nombre"]; ?></option>
					<?php
							}
						}
					?>
				</select>


				<input type="submit" value="Crear recibo" class="btn_save">
			</form>
		</div>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>