<?php

	include "../conexion.php";
	if (!empty($_POST)) {
		// code...
		$idcategoria= $_POST['id_categoria'];
		$query_delete= mysqli_query($conection, "DELETE FROM categorias WHERE id_categoria=$idcategoria");
		if ($query_delete) {
			// code...
			header('location: lista_categorias.php');
		}else{
			echo "Error al eliminar";
		}
	}

	if (empty($_REQUEST['id'])) {
		// code...
		header('location: lista_categorias.php');
	}else{
		$idcategoria = $_REQUEST['id'];

		$query= mysqli_query($conection, "SELECT nombre, precio FROM categorias WHERE id_categoria= $idcategoria");

		$result= mysqli_num_rows($query);

		if ($result > 0) {
			// code...
			while ($data= mysqli_fetch_array($query)) {
	 			// code...
	 			$nombre = $data['nombre'];
	 			$precio = $data['precio'];

			}
		}else{
			header("location: lista_categorias.php");
		}


	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "./includes/scripts.php"; ?>
	<title>Eliminar Categorias</title>
</head>
<body>
	<?php include "./includes/header.php"; ?>
	<section id="container">
		<div class="data_delete">
			<h2>¿Esta seguro de eliminar el siguiente registro?</h2>
			<p>Nombre: <span><?php echo $nombre;?></span></p>
			<p>Precio: <span><?php echo $precio;?></span></p>

			<form method="post" action="">
				<input type="hidden" name="id_categoria" value="<?php echo $idcategoria;?>">
				<a href="lista_categorias.php" class="btn_cancel">Cancelar</a>
				<input type="submit" value="Aceptar" class="btn_ok">
			</form>
		</div>
	</section>
	<?php include "./includes/footer.php"; ?>
</body>
</html>