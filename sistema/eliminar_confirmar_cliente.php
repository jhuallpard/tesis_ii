<?php

	include "../conexion.php";
	if (!empty($_POST)) {
		// code...
		$idcliente= $_POST['id_cliente'];
		$query_delete= mysqli_query($conection, "DELETE FROM clientes WHERE id_cliente=$idcliente");
		if ($query_delete) {
			// code...
			header('location: lista_clientes.php');
		}else{
			echo "Error al eliminar";
		}
	}

	if (empty($_REQUEST['id'])) {
		// code...
		header('location: lista_clientes.php');
	}else{
		$idcliente = $_REQUEST['id'];

		$query= mysqli_query($conection, "SELECT nombre, correo FROM clientes WHERE id_cliente= $idcliente");

		$result= mysqli_num_rows($query);

		if ($result > 0) {
			// code...
			while ($data= mysqli_fetch_array($query)) {
	 			// code...
	 			$nombre = $data['nombre'];
	 			$correo = $data['correo'];

			}
		}else{
			header("location: lista_clientes.php");
		}


	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "./includes/scripts.php"; ?>
	<title>Eliminar Cliente</title>
</head>
<body>
	<?php include "./includes/header.php"; ?>
	<section id="container">
		<div class="data_delete">
			<h2>¿Esta seguro de eliminar el siguiente registro?</h2>
			<p>Nombre: <span><?php echo $nombre;?></span></p>
			<p>Correo: <span><?php echo $correo;?></span></p>

			<form method="post" action="">
				<input type="hidden" name="id_cliente" value="<?php echo $idcliente;?>">
				<a href="lista_clientes.php" class="btn_cancel">Cancelar</a>
				<input type="submit" value="Aceptar" class="btn_ok">
			</form>
		</div>
	</section>
	<?php include "./includes/footer.php"; ?>
</body>
</html>