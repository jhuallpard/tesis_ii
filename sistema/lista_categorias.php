<?php
	include "../conexion.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Lista de Categorias</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<h1>Lista de Categorias</h1>
		<a href="registro_categoria.php" class="btn_new"> Crear Categoria</a>
		<table>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Precio</th>
				<th>Acciones</th>
			</tr>
			<?php

				$query= mysqli_query($conection, "SELECT id_categoria, nombre, descripcion, precio FROM categorias");
				$result= mysqli_num_rows($query);

				if ($result > 0) {
					// code...
					while ($data= mysqli_fetch_array($query)) {
						// code...
			?>
			<tr>
				<td><?php echo $data["id_categoria"]; ?></td>
				<td><?php echo $data["nombre"]; ?></td>
				<td><?php echo $data["descripcion"]; ?></td>
				<td><?php echo $data["precio"]; ?></td>
				<td>
					<a class="link_edit" href="editar_categoria.php?id=<?php echo $data["id_categoria"]; ?>">Editar</a>
					|
					<a class="link_delete" href="eliminar_confirmar_categoria.php?id=<?php echo $data["id_categoria"]; ?>">Eliminar</a>
				</td>
			</tr>

			<?php
					}
				}
			?>
		</table>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>