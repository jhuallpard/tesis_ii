<?php

	include "../conexion.php";
	if (!empty($_POST)) {
		// code...
		$idpropiedad= $_POST['id_propiedad'];
		$query_delete= mysqli_query($conection, "DELETE FROM propiedades WHERE id_propiedad=$idpropiedad");
		if ($query_delete) {
			// code...
			header('location: lista_propiedades.php');
		}else{
			echo "Error al eliminar";
		}
	}

	if (empty($_REQUEST['id'])) {
		// code...
		header('location: lista_propiedades.php');
	}else{
		$idpropiedad = $_REQUEST['id'];

		$query= mysqli_query($conection, "SELECT c.nombre, p.direccion, (cat.nombre) as nombre_cat FROM propiedades p INNER JOIN clientes c ON p.id_cliente=c.id_cliente INNER JOIN categorias cat ON p.id_categoria=cat.id_categoria WHERE id_propiedad= $idpropiedad");

		$result= mysqli_num_rows($query);

		if ($result > 0) {
			// code...
			while ($data= mysqli_fetch_array($query)) {
	 			// code...
	 			$nombre = $data['nombre'];
	 			$direccion = $data['direccion'];
	 			$categoria = $data['nombre_cat'];

			}
		}else{
			header("location: lista_propiedades.php");
		}


	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "./includes/scripts.php"; ?>
	<title>Eliminar Propiedad</title>
</head>
<body>
	<?php include "./includes/header.php"; ?>
	<section id="container">
		<div class="data_delete">
			<h2>¿Esta seguro de eliminar el siguiente registro?</h2>
			<p>Propietario: <span><?php echo $nombre;?></span></p>
			<p>Dirección: <span><?php echo $direccion;?></span></p>
			<p>Categoría: <span><?php echo $categoria;?></span></p>

			<form method="post" action="">
				<input type="hidden" name="id_propiedad" value="<?php echo $idpropiedad;?>">
				<a href="lista_propiedades.php" class="btn_cancel">Cancelar</a>
				<input type="submit" value="Aceptar" class="btn_ok">
			</form>
		</div>
	</section>
	<?php include "./includes/footer.php"; ?>
</body>
</html>