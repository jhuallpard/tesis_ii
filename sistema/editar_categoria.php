<?php
	include "../conexion.php";
	if (!empty($_POST)) {
	 	// code...
	 	$alert='';
	 	if (empty($_POST['nombre']) || empty($_POST['descripcion']) || empty($_POST['precio'])) {
	 		// code...
	 		$alert= '<p class="msg_error">Todos los campos son obligatorios</p>';
	 	}else{
	 		$idcategoria= $_POST['id_categoria'];
	 		$nombre = $_POST['nombre'];
	 		$descripcion = $_POST['descripcion'];
	 		$precio = $_POST['precio'];

	 		$query= mysqli_query($conection, "SELECT * FROM categorias WHERE (nombre= '$nombre' AND id_categoria!= $idcategoria)");

	 		$result= mysqli_fetch_array($query);

	 		if ($result > 0) {
	 			// code...
	 			$alert= '<p class="smg_error">La categoría ya existe</p>';
	 		}else{
	 			$sql_update= mysqli_query($conection, "UPDATE categorias SET nombre= '$nombre', descripcion= '$descripcion', precio= '$precio' WHERE id_categoria= '$idcategoria'");

	 			if ($sql_update) {
	 				// code...
	 				$alert= '<p class="smg_save"> Categoría actualizado correctamente</p>';
	 				header('location: lista_categorias.php');
	 			}else{
	 				$alert= '<p class="smg_save"> Error al actualizar la categoría</p>';
	 			}
	 		}
	 	}
	}

	//Mostrar Categorias
	 if (empty($_GET['id'])) {
	 	// code...
	 	header('location: lista_categorias.php');
	 }
	 $idcategoria=$_GET['id'];

	 $sql= mysqli_query($conection, "SELECT id_categoria, nombre, descripcion, precio FROM categorias WHERE id_categoria= $idcategoria");

	 $result_sql= mysqli_num_rows($sql);

	 if ($result_sql == 0) {
	 	// code...
	 	header('location: lista_categorias.php');
	 }else{
	 	while ($data= mysqli_fetch_array($sql)) {
	 		// code...
	 		$idcategoria= $data['id_categoria'];
	 		$nombre = $data['nombre'];
	 		$descripcion = $data['descripcion'];
	 		$precio = $data['precio'];
	 	}
	 }

?>


<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Actualizar Categoria</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<div class="form_register">
			<h1>Actualizar Categoria</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert:''; ?></div>

			<form action="" method="post">
				<input type="hidden" name="id_categoria" value="<?php echo $idcategoria; ?>">
				<label for="nombre">Nombre:</label>
				<input type="text" name="nombre" id="nombre" placeholder="Ingrese el nombre de la categoria" value="<?php echo $nombre; ?>">

				<label for="descripcion">Descripcion:</label>
				<!--<input type="text" name="descripcion" id="descripcion" placeholder="Ingrese la Descripción">-->
				<textarea name="descripcion" id="descripcion" placeholder="Ingrese la Descripción" rows="10" cols="40"><?php echo $descripcion; ?></textarea>

				<label for="precio">Precio:</label>
				<input type="number" name="precio" id="precio" min="0.00" max="10000.00" step="0.01" placeholder="Ingrese el Precio" value="<?php echo $precio; ?>">

				<input type="submit" value="Actualizar categoria" class="btn_save">
			</form>
		</div>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>