<?php
	include "../conexion.php";
	if (!empty($_POST)) {
	 	// code...
	 	$alert='';
	 	if (empty($_POST['direccion']) || empty($_POST['estado']) || empty($_POST['descripcion']) || empty($_POST['id_categoria']) || empty($_POST['id_sector']) || empty($_POST['id_cliente'])) {
	 		// code...
	 		$alert= '<p class="msg_error">Todos los campos son obligatorios</p>';
	 	}else{

	 		$direccion = $_POST['direccion'];
	 		$descripcion = $_POST['descripcion'];
	 		$estado = $_POST['estado'];
	 		$categoria = $_POST['id_categoria'];
	 		$sector = $_POST['id_sector'];
	 		$cliente = $_POST['id_cliente'];

	 		$query= mysqli_query($conection, "SELECT * FROM propiedades where direccion='$direccion'");
	 		$result= mysqli_fetch_array($query);

	 		if ($result > 0) {
	 			// code...
	 			$alert= '<p class="smg_error">La propiedad ya existe</p>';
	 		}else{
	 			$query_insert= mysqli_query($conection, "INSERT INTO propiedades(direccion, estado, descripcion, id_categoria, id_sector, id_cliente) VALUES ('$direccion','$estado','$descripcion','$categoria','$sector','$cliente')");
	 			if ($query_insert) {
	 				// code...
	 				$alert= '<p class="smg_save"> Propiedad creado correctamente</p>';
	 				header('location: lista_propiedades.php');
	 			}else{
	 				$alert= '<p class="smg_save"> Error al crear la Propiedad</p>';
	 			}
	 		}
	 	}
	 	//mysql_close($conection);
	 } 
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Registro Propiedad</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<div class="form_register">
			<h1>Registro Propiedad</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert:''; ?></div>

			<form action="" method="post">
				<label for="direccion">Dirección:</label>
				<input type="text" name="direccion" id="direccion" placeholder="Ingrese la Dirección">

				<label for="descripcion">Descripcion:</label>
				<textarea name="descripcion" id="descripcion" placeholder="Ingrese la Descripción" rows="10" cols="40"></textarea>

				<label for="id_categoria">Nombre de Categoria:</label>
				<?php
					$query_categoria= mysqli_query($conection, "SELECT * FROM categorias");
					$result_categoria= mysqli_num_rows($query_categoria);
				?>

				<select name="id_categoria" id="id_categoria">
					<?php 
						if ($result_categoria > 0) {
							while ($categoria = mysqli_fetch_array($query_categoria)) {
					?>
								<option value="<?php echo $categoria["id_categoria"]; ?>"><?php echo $categoria["nombre"]; ?></option>
					<?php
							}
						}
					?>
				</select>

				<label for="id_sector">Nombre de Sector:</label>
				<?php
					$query_sector= mysqli_query($conection, "SELECT * FROM sectores");
					$result_sector= mysqli_num_rows($query_sector);
				?>

				<select name="id_sector" id="id_sector">
					<?php 
						if ($result_sector > 0) {
							while ($sector = mysqli_fetch_array($query_sector)) {
					?>
								<option value="<?php echo $sector["id_sector"]; ?>"><?php echo $sector["nombre"]; ?></option>
					<?php
							}
						}
					?>
				</select>

				<label for="id_cliente">Propiedario:</label>
				<?php
					$query_cliente= mysqli_query($conection, "SELECT * FROM clientes");
					$result_cliente= mysqli_num_rows($query_cliente);
				?>

				<select name="id_cliente" id="id_cliente">
					<?php 
						if ($result_cliente > 0) {
							while ($cliente = mysqli_fetch_array($query_cliente)) {
					?>
								<option value="<?php echo $cliente["id_cliente"]; ?>"><?php echo $cliente["nombre"]; ?></option>
					<?php
							}
						}
					?>
				</select>

				<label for="estado">Estado:</label>

				<select name="estado" id="estado">
					<option value="activo">Activo</option>
					<option value="inactivo">Inactivo</option>
				</select>

				<input type="submit" value="Crear cliente" class="btn_save">
			</form>
		</div>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>