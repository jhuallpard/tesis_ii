<?php
	include "../conexion.php";
	if (!empty($_POST)) {
	 	// code...
	 	$alert='';
	 	if (empty($_POST['nombre']) || empty($_POST['celular']) || empty($_POST['correo']) || empty($_POST['dni'])) {
	 		// code...
	 		$alert= '<p class="msg_error">Todos los campos son obligatorios</p>';
	 	}else{

	 		$nombre = $_POST['nombre'];
	 		$email = $_POST['correo'];
	 		$celular = $_POST['celular'];
	 		$dni = $_POST['dni'];

	 		$query= mysqli_query($conection, "SELECT * FROM clientes where nombre='$nombre' OR correo='$email'");
	 		$result= mysqli_fetch_array($query);

	 		if ($result > 0) {
	 			// code...
	 			$alert= '<p class="smg_error">El cliente ya existe</p>';
	 		}else{
	 			$query_insert= mysqli_query($conection, "INSERT INTO clientes(nombre,correo,celular,dni) VALUES ('$nombre','$email','$celular','$dni')");
	 			if ($query_insert) {
	 				// code...
	 				$alert= '<p class="smg_save"> Cliente creado correctamente</p>';
	 				header('location: lista_clientes.php');
	 			}else{
	 				$alert= '<p class="smg_save"> Error al crear el Cliente</p>';
	 			}
	 		}
	 	}
	 	//mysql_close($conection);
	 } 
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<style>
		/*Input type number sin flecha*/

		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button { 
		  -webkit-appearance: none; 
		  margin: 0; 
		}

		input[type=number] { -moz-appearance:textfield; }
	</style>
	<title>Registro Cliente</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<div class="form_register">
			<h1>Registro Cliente</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert:''; ?></div>

			<form action="" method="post">
				<label for="nombre">Nombre:</label>
				<input type="text" name="nombre" id="nombre" placeholder="Ingrese el nombre Completo">

				<label for="correo">Correo electrónico:</label>
				<input type="email" name="correo" id="correo" placeholder="Ingrese el Correo Electrónico">

				<label for="celular">Celular:</label>
				<input type="number" maxlength="9" minlength="9" name="celular" id="celular" placeholder="Ingrese el nro celular">

				<label for="dni">DNI:</label>
				<input type="number" name="dni" maxlength="8" minlength="8" id="dni" placeholder="Ingrese el nro de DNI">

				<input type="submit" value="Crear cliente" class="btn_save">
			</form>
		</div>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>