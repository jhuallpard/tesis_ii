<?php

	include "../conexion.php";
	if (!empty($_POST)) {
		// code...
		$idrecibo= $_POST['id_recibo'];
		$query_delete= mysqli_query($conection, "DELETE FROM recibos WHERE id_recibo=$idrecibo");
		if ($query_delete) {
			// code...
			header('location: lista_recibos.php');
		}else{
			echo "Error al eliminar";
		}
	}

	if (empty($_REQUEST['id'])) {
		// code...
		header('location: lista_recibos.php');
	}else{
		$idrecibo = $_REQUEST['id'];

		$query= mysqli_query($conection, "SELECT r.id_recibo, (c.nombre)as nombre_cliente, p.direccion, (cat.nombre) as nombre_categoria  FROM recibos r 
			INNER JOIN propiedades p ON r.id_propiedad=p.id_propiedad 
			INNER JOIN clientes c ON p.id_cliente=c.id_cliente
			INNER JOIN categorias cat ON p.id_categoria=cat.id_categoria 
			WHERE r.id_recibo= $idrecibo");

		$result= mysqli_num_rows($query);

		if ($result > 0) {
			// code...
			while ($data= mysqli_fetch_array($query)) {
	 			// code...
	 			$nombre_cli = $data['nombre_cliente'];
	 			$direccion = $data['direccion'];
	 			$nombre_cat = $data['nombre_categoria'];

			}
		}else{
			header("location: lista_recibos.php");
		}


	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "./includes/scripts.php"; ?>
	<title>Eliminar Categorias</title>
</head>
<body>
	<?php include "./includes/header.php"; ?>
	<section id="container">
		<div class="data_delete">
			<h2>¿Esta seguro de eliminar el siguiente registro?</h2>
			<p>Cliente: <span><?php echo $nombre_cli;?></span></p>
			<p>Dirección: <span><?php echo $direccion;?></span></p>
			<p>Categoria: <span><?php echo $nombre_cat;?></span></p>

			<form method="post" action="">
				<input type="hidden" name="id_recibo" value="<?php echo $idrecibo;?>">
				<a href="lista_recibos.php" class="btn_cancel">Cancelar</a>
				<input type="submit" value="Aceptar" class="btn_ok">
			</form>
		</div>
	</section>
	<?php include "./includes/footer.php"; ?>
</body>
</html>