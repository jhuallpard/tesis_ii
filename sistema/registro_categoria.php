<?php
	include "../conexion.php";
	if (!empty($_POST)) {
	 	// code...
	 	$alert='';
	 	if (empty($_POST['nombre']) || empty($_POST['descripcion']) || empty($_POST['precio'])) {
	 		// code...
	 		$alert= '<p class="msg_error">Todos los campos son obligatorios</p>';
	 	}else{

	 		$nombre = $_POST['nombre'];
	 		$descripcion = $_POST['descripcion'];
	 		$precio = $_POST['precio'];

	 		$query= mysqli_query($conection, "SELECT * FROM categorias where nombre='$nombre'");
	 		$result= mysqli_fetch_array($query);

	 		if ($result > 0) {
	 			// code...
	 			$alert= '<p class="smg_error">La categoria ya existe</p>';
	 		}else{
	 			$query_insert= mysqli_query($conection, "INSERT INTO categorias(nombre,descripcion,precio) VALUES ('$nombre','$descripcion','$precio')");
	 			if ($query_insert) {
	 				// code...
	 				$alert= '<p class="smg_save"> Categoria creado correctamente</p>';
	 				header('location: lista_categorias.php');
	 			}else{
	 				$alert= '<p class="smg_save"> Error al crear la Categoria</p>';
	 			}
	 		}
	 	}
	 	//mysql_close($conection);
	 } 
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Registro Categoria</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<div class="form_register">
			<h1>Registro Categoria</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert:''; ?></div>

			<form action="" method="post">
				<label for="nombre">Nombre:</label>
				<input type="text" name="nombre" id="nombre" placeholder="Ingrese el nombre de la categoria">

				<label for="descripcion">Descripcion:</label>
				<!--<input type="text" name="descripcion" id="descripcion" placeholder="Ingrese la Descripción">-->
				<textarea name="descripcion" id="descripcion" placeholder="Ingrese la Descripción" rows="10" cols="40"></textarea>

				<label for="precio">Precio:</label>
				<input type="number" name="precio" id="precio" min="0.00" max="10000.00" step="0.01" placeholder="Ingrese el Precio">

				<input type="submit" value="Crear categoria" class="btn_save">
			</form>
		</div>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>