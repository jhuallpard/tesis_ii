<?php
	include "../conexion.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Lista de Clientes</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<h1>Lista de Clientes</h1>
		<a href="registro_cliente.php" class="btn_new"> Crear Cliente</a>
		<table>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Nro Celular</th>
				<th>DNI</th>
				<th>Acciones</th>
			</tr>
			<?php

				$query= mysqli_query($conection, "SELECT id_cliente, nombre, correo, celular, dni FROM clientes");
				$result= mysqli_num_rows($query);

				if ($result > 0) {
					// code...
					while ($data= mysqli_fetch_array($query)) {
						// code...
			?>
			<tr>
				<td><?php echo $data["id_cliente"]; ?></td>
				<td><?php echo $data["nombre"]; ?></td>
				<td><?php echo $data["correo"]; ?></td>
				<td><?php echo $data["celular"]; ?></td>
				<td><?php echo $data["dni"]; ?></td>
				<td>
					<a class="link_edit" href="editar_cliente.php?id=<?php echo $data["id_cliente"]; ?>">Editar</a>
					|
					<a class="link_delete" href="eliminar_confirmar_cliente.php?id=<?php echo $data["id_cliente"]; ?>">Eliminar</a>
				</td>
			</tr>

			<?php
					}
				}
			?>
		</table>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>