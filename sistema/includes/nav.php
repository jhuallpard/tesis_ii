<nav>
			<ul>
				<li><a href="index.php">Inicio</a></li>
				<li class="principal">
					<a href="registro_usuario.php">Usuarios</a>
					<ul>
						<li><a href="registro_usuario.php">Nuevo Usuario</a></li>
						<li><a href="lista_usuarios.php">Lista de Usuarios</a></li>
					</ul>
				</li>
				<li class="principal">
					<a href="registro_cliente.php">Clientes</a>
					<ul>
						<li><a href="registro_cliente.php">Nuevo Cliente</a></li>
						<li><a href="lista_clientes.php">Lista de Clientes</a></li>
					</ul>
				</li>
				<li class="principal">
					<a href="registro_propiedad.php">Domicilio</a>
					<ul>
						<li><a href="registro_propiedad.php">Nueva Propiedad</a></li>
						<li><a href="lista_propiedades.php">Lista de Propiedades</a></li>
					</ul>
				</li>
				<li class="principal">
					<a href="registro_categoria.php">Categoria</a>
					<ul>
						<li><a href="registro_categoria.php">Nuevo Categoria</a></li>
						<li><a href="lista_categorias.php">Lista de Categoria</a></li>
					</ul>
				</li>
				<li class="principal">
					<a href="registro_recibo.php">Facturas</a>
					<ul>
						<li><a href="registro_recibo.php">Nuevo Factura</a></li>
						<li><a href="lista_recibos.php">Facturas</a></li>
					</ul>
				</li>
			</ul>
		</nav>