<?php
	include "../conexion.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<title>Lista de Categorias</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<h1>Lista de Recibos</h1>
		<a href="registro_recibo.php" class="btn_new"> Crear Recibos</a>
		<table>
			<tr>
				<th>ID</th>
				<th>Cliente</th>
				<th>Dirección</th>
				<th>Mes</th>
				<th>Categoria</th>
				<th>Precio</th>
				<th>Acciones</th>
			</tr>
			<?php

				$query= mysqli_query($conection, "select r.id_recibo, (c.nombre)as nombre_cliente, p.direccion,  r.mes, (cat.nombre) as nombre_categoria, cat.precio
					FROM recibos r 
					INNER JOIN propiedades p ON r.id_propiedad=p.id_propiedad
					INNER JOIN clientes c ON p.id_cliente=c.id_cliente
					INNER JOIN categorias cat ON p.id_categoria=cat.id_categoria");
				$result= mysqli_num_rows($query);

				if ($result > 0) {
					// code...
					while ($data= mysqli_fetch_array($query)) {
						// code...
			?>
			<tr>
				<td><?php echo $data["id_recibo"]; ?></td>
				<td><?php echo $data["nombre_cliente"]; ?></td>
				<td><?php echo $data["direccion"]; ?></td>
				<td><?php echo $data["mes"]; ?></td>
				<td><?php echo $data["nombre_categoria"]; ?></td>
				<td><?php echo "S/. ",$data["precio"]; ?></td>
				<td>
					<a class="link_edit" href="editar_categoria.php?id=<?php echo $data["id_recibo"]; ?>">Detalles</a>
					|
					<a class="link_delete" href="eliminar_confirmar_recibo.php?id=<?php echo $data["id_recibo"]; ?>">Eliminar</a>
				</td>
			</tr>

			<?php
					}
				}
			?>
		</table>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>