<?php
	include "../conexion.php";
	if (!empty($_POST)) {
	 	// code...
	 	$alert='';
	 	if (empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['celular']) || empty($_POST['dni'])) {
	 		// code...
	 		$alert= '<p class="msg_error">Todos los campos son obligatorios</p>';
	 	}else{
	 		$idcliente= $_POST['id_cliente'];
	 		$nombre = $_POST['nombre'];
	 		$correo = $_POST['correo'];
	 		$celular = $_POST['celular'];
	 		$dni = $_POST['dni'];

	 		$query= mysqli_query($conection, "SELECT * FROM clientes WHERE (nombre= '$nombre' AND id_cliente!= $idcliente) OR (correo= '$correo' AND id_cliente!= $idcliente)");

	 		$result= mysqli_fetch_array($query);

	 		if ($result > 0) {
	 			// code...
	 			$alert= '<p class="smg_error">El cliente ya existe</p>';
	 		}else{
	 			$sql_update= mysqli_query($conection, "UPDATE clientes SET nombre= '$nombre', correo= '$correo', celular= '$celular', dni='$dni' WHERE id_cliente= '$idcliente'");

	 			if ($sql_update) {
	 				// code...
	 				$alert= '<p class="smg_save"> Cliente actualizado correctamente</p>';
	 				header('location: lista_clientes.php');
	 			}else{
	 				$alert= '<p class="smg_save"> Error al actualizar el Cliente</p>';
	 			}
	 		}
	 	}
	}

	//Mostrar Clientes
	 if (empty($_GET['id'])) {
	 	// code...
	 	header('location: lista_clientes.php');
	 }
	 $idcliente=$_GET['id'];

	 $sql= mysqli_query($conection, "SELECT id_cliente, nombre, correo, celular, dni FROM clientes WHERE id_cliente= $idcliente");

	 $result_sql= mysqli_num_rows($sql);

	 if ($result_sql == 0) {
	 	// code...
	 	header('location: lista_clientes.php');
	 }else{
	 	while ($data= mysqli_fetch_array($sql)) {
	 		// code...
	 		$idcliente= $data['id_cliente'];
	 		$nombre = $data['nombre'];
	 		$correo = $data['correo'];
	 		$celular = $data['celular'];
	 		$dni = $data['dni'];
	 	}
	 }

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php"; ?>
	<style>
		/*Input type number sin flecha*/

		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button { 
		  -webkit-appearance: none; 
		  margin: 0; 
		}

		input[type=number] { -moz-appearance:textfield; }
	</style>
	<title>Actualizar Cliente</title>
</head>
<body>
	<?php include "includes/header.php"; ?>
	<section id="container">
		<div class="form_register">
			<h1>Actualizar Cliente</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert:''; ?></div>

			<form action="" method="post">
				<input type="hidden" name="id_cliente" value="<?php echo $idcliente; ?>">
				<label for="nombre">Nombre:</label>
				<input type="text" name="nombre" id="nombre" placeholder="Ingrese el nombre Completo" value="<?php echo $nombre; ?>">

				<label for="correo">Correo electrónico:</label>
				<input type="email" name="correo" id="correo" placeholder="Ingrese el Correo Electrónico" value="<?php echo $correo; ?>">

				<label for="celular">Celular:</label>
				<input type="number" maxlength="9" minlength="9" name="celular" id="celular" placeholder="Ingrese el nro celular" value="<?php echo $celular; ?>">

				<label for="dni">DNI:</label>
				<input type="number" name="dni" maxlength="8" minlength="8" id="dni" placeholder="Ingrese el nro de DNI" value="<?php echo $dni; ?>">

				<input type="submit" value="Actualizar cliente" class="btn_save">
			</form>
		</div>
	</section>
	<?php include "includes/footer.php"; ?>
</body>
</html>